import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './photo.dart';

class GalleryProvider with ChangeNotifier {
  List<Photo> _photos = [];

  List<Photo> get photos {
    return [..._photos];
  }

  Photo findById(String id) {
    return _photos.firstWhere((prod) => prod.id == id);
  }

  Future<List<Photo>> fetchAndSetGallery() async {
    const url =
        'https://api.unsplash.com/photos/?client_id=896d4f52c589547b2134bd75ed48742db637fa51810b49b607e37e46ab2c0043';
    try {
      final res = await http.get(url);
      if (res.statusCode == 200) {
        List<Photo> list = parsePhotos(res.body);
        list.forEach((photo) {
          photos.add(
            Photo(
              id: photo.id,
              description: photo.description,
              imgUrl: photo.imgUrl,
              thumbUrl: photo.thumbUrl,
              userName: photo.userName,
            ),
          );
        });
        _photos = list;
      } else {
        throw Exception('error');
      }
    } catch (e) {
      throw Exception(e.toString());
    }

    notifyListeners();
  }

  List<Photo> parsePhotos(String resBody) {
    final parsed = json.decode(resBody).cast<Map<String, dynamic>>();
    return parsed.map<Photo>((json) => Photo.fromJson(json)).toList();
  }
}
