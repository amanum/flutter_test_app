import 'package:flutter/material.dart';

class Photo with ChangeNotifier {
  final String id;
  final String description;
  final String thumbUrl;
  final String imgUrl;
  final String userName;

  Photo({
    @required this.id,
    @required this.description,
    @required this.thumbUrl,
    @required this.imgUrl,
    @required this.userName,
  });

  factory Photo.fromJson(Map<String, dynamic> json) {
    return new Photo(
      id: json['id'],
      description: json['alt_description'],
      thumbUrl: json['urls']['thumb'],
      imgUrl: json['urls']['small'],
      userName: json['user']['name'],
    );
  }
}
