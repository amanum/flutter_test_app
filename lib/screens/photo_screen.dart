import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/gallery_provider.dart';

class PhotoScreen extends StatefulWidget {
  static const routeName = 'photo-screen';

  @override
  _PhotoScreenState createState() => _PhotoScreenState();
}

class _PhotoScreenState extends State<PhotoScreen> {
  var _isInit = true;
  var _isLoading = false;
  double _imageHeight;

  Future<void> _getImageHeight(String imgUrl) {
    Image image = Image.network(imgUrl);
    image.image.resolve(ImageConfiguration()).addListener(
      ImageStreamListener(
        (ImageInfo image, bool synchronousCall) {
          var myImage = image.image;
          var screenWidth = MediaQuery.of(context).size.width;
          double loadedImageHeight = _imageHeight = myImage.height.toDouble();
          double loadedImageWidth = _imageHeight = myImage.width.toDouble();
          var finalHeight =
              screenWidth * loadedImageHeight / loadedImageWidth - 20;
          setState(() {
            _imageHeight = finalHeight;
          });
        },
      ),
    );
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      final photoId = ModalRoute.of(context).settings.arguments as String;
      final loadedPhoto = Provider.of<GalleryProvider>(context, listen: false)
          .findById(photoId);
      _getImageHeight(loadedPhoto.imgUrl);
      setState(() {
        _isLoading = false;
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final photoId = ModalRoute.of(context).settings.arguments as String;
    final loadedPhoto =
        Provider.of<GalleryProvider>(context, listen: false).findById(photoId);
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  expandedHeight: _imageHeight,
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                    title: Text(photoId),
                    background: Hero(
                      tag: photoId,
                      child: Image.network(
                        loadedPhoto.imgUrl,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate([
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Author: ${loadedPhoto.userName}',
                      style: TextStyle(color: Colors.grey, fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      width: double.infinity,
                      child: Text(
                        loadedPhoto.description != null
                            ? 'description: ${loadedPhoto.description}'
                            : 'no description',
                        textAlign: TextAlign.center,
                        softWrap: true,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: Text(
                        'Image :',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: Image.network(
                        loadedPhoto.imgUrl,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      height: 500,
                      child: Center(
                        child: Text(
                            'Контейнер с высотой 500px для показа анимации хедера'),
                      ),
                    )
                  ]),
                ),
              ],
            ),
    );
  }
}
