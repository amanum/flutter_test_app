import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/gallery_provider.dart';
import '../widgets/photo_item.dart';

class GalleryScreen extends StatefulWidget {
  @override
  _GalleryScreenState createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<GalleryProvider>(context, listen: false)
          .fetchAndSetGallery()
          .then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  Future<void> _refreshTransactions() async {
    await Provider.of<GalleryProvider>(context).fetchAndSetGallery();
  }

  @override
  Widget build(BuildContext context) {
    final photos = Provider.of<GalleryProvider>(context).photos;
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('Gallery'),
        elevation: 0.0,
      ),
      body: RefreshIndicator(
        onRefresh: _refreshTransactions,
        child: Container(
          margin: EdgeInsets.only(left: 10, right: 10),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Color(0xFF61637D),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
            ),
          ),
          child: _isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : GridView.builder(
                  padding: const EdgeInsets.all(10.0),
                  itemCount: photos.length,
                  itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
                    value: photos[i],
                    child: PhotoItem(),
                  ),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                  ),
                ),
        ),
      ),
    );
  }
}
