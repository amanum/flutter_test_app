import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/photo.dart';
import '../screens/photo_screen.dart';

class PhotoItem extends StatelessWidget {
  // final String id;
  // final String description;
  // final String thumbUrl;
  // final String imgUrl;
  // final String userName;

  // PhotoItem({
  //   @required this.id,
  //   @required this.description,
  //   @required this.thumbUrl,
  //   @required this.imgUrl,
  //   @required this.userName,
  // });

  @override
  Widget build(BuildContext context) {
    final photo = Provider.of<Photo>(context, listen: false);
    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: GridTile(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context)
                .pushNamed(PhotoScreen.routeName, arguments: photo.id);
          },
          child: Stack(
            children: <Widget>[
              Hero(
                tag: photo.id,
                child: Container(
                  width: double.infinity,
                  child: FadeInImage(
                    placeholder: AssetImage('assets/img/imagePlaceholder.png'),
                    image: NetworkImage(photo.imgUrl),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Positioned(
                top: 0,
                right: 0,
                left: 0,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.black38,
                  ),
                  padding: EdgeInsets.symmetric(
                    vertical: 5,
                    horizontal: 10,
                  ),
                  child: FittedBox(
                    child: Text(
                      photo.userName,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: Container(
                  color: Colors.black45,
                  padding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 15,
                  ),
                  child: Text(
                    photo.id,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
