import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_test_app/screens/gallery_screen.dart';

import './providers/gallery_provider.dart';
import './screens/photo_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: GalleryProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Color(0xFF31334D),
        ),
        home: GalleryScreen(),
        routes: {
          PhotoScreen.routeName: (ctx) => PhotoScreen(),
        },
      ),
    );
  }
}
